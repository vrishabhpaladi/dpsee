package com.pesit.vrishabh;

public class TextTShirt extends TShirtDecorator
{
	protected String value;
	public TextTShirt(TShirt tShirt,String value)
	{
		super(tShirt);
		this.value=value;
	}
	@Override
	public String getDiscription()
	{
		return super.getDiscription()+" Text:"+value;
	}

	@Override
	public int getCost() {
		return super.getCost()+60;
	}

}
