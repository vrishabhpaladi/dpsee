package com.pesit.vrishabh;
public class TShirtDecorator implements TShirt
{
	protected TShirt tshirt;
	public TShirtDecorator(TShirt tShirt)
	{
		this.tshirt=tShirt;
	}
	@Override
	public String getDiscription()
	{
		return tshirt.getDiscription();
	}

	@Override
	public int getCost() {
		return tshirt.getCost();
	}
	
}
