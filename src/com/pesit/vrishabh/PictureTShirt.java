package com.pesit.vrishabh;

public class PictureTShirt extends TShirtDecorator
{
	protected String value;
	public PictureTShirt(TShirt tShirt,String value)
	{
		super(tShirt);
		this.value=value;
	}
	@Override
	public String getDiscription()
	{
		return super.getDiscription()+" Picture:"+value;
	}

	@Override
	public int getCost() {
		return super.getCost()+70;
	}

}
