package com.pesit.vrishabh;

public class BackGroundColorTShirt extends TShirtDecorator
{
	protected String value;
	public BackGroundColorTShirt(TShirt tShirt,String value)
	{
		super(tShirt);
		this.value=value;
	}
	@Override
	public String getDiscription()
	{
		return super.getDiscription()+" BackGround Color:"+value;
	}

	@Override
	public int getCost() {
		return super.getCost()+50;
	}

}
